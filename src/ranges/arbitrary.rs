use arbitrary::{Arbitrary, Result, Unstructured};

use crate::{Domain, GenericRange, Ranges};

impl<T: Arbitrary + Domain> Arbitrary for Ranges<T> {
    fn arbitrary(u: &mut Unstructured<'_>) -> Result<Self> {
        let len = u.arbitrary_len::<GenericRange<T>>()?;
        let mut vec = Ranges::with_capacity(len);

        for _ in 0..len {
            let _ = vec.insert(u.arbitrary::<GenericRange<T>>()?);
        }

        Ok(vec)
    }
}
