use criterion::{black_box, criterion_group, criterion_main, BatchSize, BenchmarkId, Criterion, Throughput};

use ranges::{GenericRange, Ranges};

fn insert(c: &mut Criterion) {
    let mut group = c.benchmark_group("insert collisions");
    let max_exponent = 14;

    for exponent in 0..=max_exponent {
        let mut ranges = Ranges::new();
        for i in 0..2_usize.pow(exponent) {
            ranges.insert(i * 10..(i * 10) + 5);
        }

        group.throughput(Throughput::Elements(ranges.len() as u64));
        group.bench_function(BenchmarkId::from_parameter(ranges.len()), |b| {
            b.iter_batched(
                || ranges.clone(),
                |mut r| {
                    let _ = r.insert(black_box(GenericRange::from(usize::min_value()..=usize::max_value())));
                },
                BatchSize::PerIteration,
            );
        });
    }

    group.finish();
}

criterion_group!(benches, insert);
criterion_main!(benches);
